/*
     This file is part of GNUnet.
     Copyright (C) 20xx GNUnet e.V.

     GNUnet is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 3, or (at your
     option) any later version.

     GNUnet is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with GNUnet; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
     Boston, MA 02110-1301, USA.
*/

/**
 * @file ext/ext_api.c
 * @brief API for ext
 * @author
 */
#include "gnunet_ext_config.h"
#include <stddef.h>

#include <gnunet/gnunet_util_lib.h>
#include "gnunet_ext_service.h"

void
EXT_TEST_new_func (void* intarg)
{
  return;
}

/* end of ext_api.c */
